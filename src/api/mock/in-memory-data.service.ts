import {Injectable} from '@angular/core';
import {InMemoryDbService} from "angular-in-memory-web-api";
import Recipe from "../../app/recipes/recipe.model";

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  constructor() {}

  createDb() {
    // const recipes = {
    //   id: 1,
    //   data: [
    //     new Recipe(0, 'test', 'test2', 'dfhdfh', [])
    //   ]
    // };
    const recipes = [
      new Recipe(0, 'test', 'test2', 'dfhdfh', [])
    ];

    const signup = [
      {
        id: 1,
        success: false
      }
    ];

    const signin = {
      token: 'sdgdh'
    };

    return { recipes, signup, signin };
  }
}
