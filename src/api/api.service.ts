import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/internal/Observable";

import {RecipeService} from "../app/recipes/recipe.service";
import Recipe from "../app/recipes/recipe.model";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient,
    private recipeService: RecipeService) { }

  storeRecipes(): Observable<any> {
    return this.http.put('api/recipes', this.recipeService.getRecipes(), httpOptions);
  }

  getRecipes() {
    this.http.get<Recipe[]>('api/recipes')
      .subscribe(
      (response) => {
        this.recipeService.setRecipes(response);
      }
    );
  }

  signup(email: string, password: string) {
    return this.http.post('api/signup', {email, password});
  }

  signin(email: string, password: string) {
    return this.http.get('api/signin');
  }
}
