import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import {HttpClientModule} from "@angular/common/http";
import {InMemoryDataService} from "./mock/in-memory-data.service";
import {HttpClientInMemoryWebApiModule} from "angular-in-memory-web-api";
import {environment} from "../environments/environment";

@NgModule({
  imports: [
    HttpClientModule,
    // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
    // and returns simulated server responses.
    // Remove it when a real server is ready to receive requests.
    !environment.production ?
      HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { delay: 500, dataEncapsulation: false }
    ): [],
    CommonModule
  ]
})
export class ApiModule { }
