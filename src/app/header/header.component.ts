import {Component, OnInit} from '@angular/core';
import {ApiService} from "../../api/api.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private apiService: ApiService) { }

  ngOnInit() {
  }

  onSaveData() {
    this.apiService.storeRecipes()
      .subscribe(
        (response) => {
          console.log(response);
        }
      )
  }

  onFetchData() {
    this.apiService.getRecipes();
  }

}
