import Recipe from "./recipe.model";
import Ingredient from "../shared/ingredient.model";
import { Subject } from "rxjs/internal/Subject";

export class RecipeService {
  recipesChanged = new Subject<Recipe[]>();
  private recipes: Recipe[] = [
    new Recipe(
      1,
      'Burger',
      'A tasty burger',
      '//www.seriouseats.com/recipes/images/2015/07/20150728-homemade-whopper-food-lab-35-1500x1125.jpg',
      [
        new Ingredient('Meat', 1),
        new Ingredient('Bread', 2)
      ]),
    new Recipe(
      2,
      'Eggs',
      'Tasty eggs',
      '//imagesvc.timeincapp.com/v3/mm/image?url=http%3A%2F%2Fcdn-image.myrecipes.com%2Fsites%2Fdefault%2Ffiles%2Fstyles%2Fmedium_2x%2Fpublic%2Ffield%2Fimage%2Fsunny-side-up-eggs-hero.jpg%3Fitok%3DMKkEO-lv&w=700&q=85',
      [
        new Ingredient('Eggs', 10),
        new Ingredient('Bacon', 1)
      ])
  ];

  setRecipes(recipes: Recipe[]) {
    this.recipes = recipes;
    this.recipesChanged.next([...this.recipes])
  }

  getRecipes() {
    return [...this.recipes];
  }

  getRecipe(id: number): Recipe {
    const recipe = this.recipes.filter(
      (recipe) => recipe.id === id
    );
    return recipe[0];
  }

  updateRecipe(id: number, newRecipe: Recipe) {
    let recipeToUpdate = this.getRecipe(id);
    let position = this.recipes.indexOf(recipeToUpdate);
    this.recipes[position] = newRecipe;

    this.recipesChanged.next([...this.recipes])
  }

  addRecipe(newRecipe: Recipe) {
    this.recipes.push(newRecipe);

    this.recipesChanged.next([...this.recipes])
  }

  rmRecipe(id: number) {
    let recipeToDel = this.getRecipe(id);
    let position = this.recipes.indexOf(recipeToDel);

    this.recipes.splice(position, 1);
    this.recipesChanged.next([...this.recipes])
  }
}
