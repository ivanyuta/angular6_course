import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";

import Recipe from "../recipe.model";
import {ShoppingListService} from "../../shopping-list/shopping-list.service";
import {RecipeService} from "../recipe.service";


@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.scss']
})
export class RecipeDetailComponent implements OnInit {
  constructor(
    private slService: ShoppingListService,
    private recipeService: RecipeService,
    private route: ActivatedRoute,
    private router: Router) { }

  recipe: Recipe;
  id: number;

  ngOnInit(): void {
    this.getRecipe();
  }

  moveToShoppingList(): void {
    this.slService.addIngredients(this.recipe.ingredients);
  }

  getRecipe(): void {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.recipe = this.recipeService.getRecipe(this.id);
        }
      )
  }

  deleteRecipe(id: number) {
    this.recipeService.rmRecipe(id);
    this.router.navigate(['/recipes']);
  }

}
