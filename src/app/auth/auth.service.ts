import {ApiService} from "../../api/api.service";
import {Injectable} from "@angular/core";

@Injectable()
export class AuthService {
  constructor(private api: ApiService) {}

  private state = {
    token: ''
  };

  get token() {
    return this.state.token;
  }

  signupUser(email: string, password: string) {
    this.api.signup(email, password)
      .subscribe(
        (response: Response) => {
          console.log(response);
        }
      )
  }

  signinUser(email: string, password: string) {
    this.api.signin(email, password)
      .subscribe(
        (response: any) => {
          if (response.token) {
            this.state.token = response.token;
          }
        }
      )
  }
}
