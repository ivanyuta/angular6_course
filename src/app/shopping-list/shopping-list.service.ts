import Ingredient from "../shared/ingredient.model";
import {Subject} from "rxjs/internal/Subject";

export class ShoppingListService {
  ingredientAdded = new Subject<Ingredient[]>();
  startedEditing = new Subject<number>();

  private ingredients: Ingredient[] = [
    new Ingredient('Apples', 5),
    new Ingredient('Tomatoes', 10)
  ];

  getIngredients() {
    return [...this.ingredients];
  }

  getIngredient(index: number) {
    return this.ingredients[index];
  }

  addIngredient(ingredient: Ingredient) {
    this.ingredients.push(ingredient);
    this.ingredientAdded.next([...this.ingredients]);
  }

  rmIngredient(index: number) {
    this.ingredients.splice(index, 1);
    this.ingredientAdded.next([...this.ingredients]);
  }

  updateIngredient(index: number, newIngredient: Ingredient) {
    this.ingredients[index] = newIngredient;
    this.ingredientAdded.next([...this.ingredients]);
  }

  //we need this method 'cause we don't want to emit change evts for every item in arr (if we've used addIngredient method here)
  addIngredients(ingredients: Ingredient[]) {
    this.ingredients.push(...ingredients);
    this.ingredientAdded.next([...this.ingredients]);
  }
}
